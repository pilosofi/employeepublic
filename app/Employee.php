<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Employee extends Model
{
    use SoftDeletes;
    
    //
    protected $fillable = [
        'name',
        'salary',
        'position',
        'gender',
        'skills',
        'birthdate'
    ];
    public function getAgeAttribute()
    {
      return Carbon::parse($this->attributes['birthdate'])->age;
    }

}
