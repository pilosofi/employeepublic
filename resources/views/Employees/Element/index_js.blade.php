$(document).ready(function() {
    $('#table-employee').DataTable();
} );

var ctx = document.getElementById("positionChart");
var positionChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: {!!$labels!!},
        datasets: [{
            label: 'Count Employee Per Position',
            data: {!!$values!!},
            backgroundColor: {!!$colors!!},
            borderColor: {!!$colors!!},
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
