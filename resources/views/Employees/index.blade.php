@extends('layouts.template')

@section('content')
    <div class="container-body">
        @if(session()->get('success'))
            <div class="alert alert-success">
              {{ session()->get('success') }}
            </div><br />
        @endif
        <div class="row" style="margin-bottom:10px">
            <canvas id="positionChart" width="400" height="100"></canvas>
        </div>
        <div class="row" style="margin-bottom: 5px;">
            <div class="col-lg-4">
                <a class="btn btn-primary" href="{{ url('/employees/create') }}">Add Employee</a>
            </div>
            <div class="col-lg-8 text-right">
                <h3>Total: {{$employee_counter['total']}}</h3>

                    @foreach($employee_counter['per_position'] as $pos)
                        {{$pos->position}} : {{$pos->count_position}} <br/>
                    @endforeach

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table id="table-employee" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Gender</th>
                            <th>Skills</th>
                            <th>Age</th>
                            <th>Salary</th>
                            <th style="width:120px">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($employees as $employee)
                            <tr>
                                <td>{{$employee->name}}</td>
                                <td>{{$employee->position}}</td>
                                <td>{{$employee->gender}}</td>
                                <td>
                                    <?php $skills = json_decode($employee->skills, TRUE) ?>
                                    @if(is_array($skills))
                                        @foreach($skills as $skill)
                                            <span class="badge badge-success">{{$skill}}</span>
                                        @endforeach
                                    @else
                                        <span class="badge badge-danger">No Skill WTF</span>
                                    @endif
                                </td>
                                <td>{{$employee->age}}</td>
                                <td>200000</td>
                                <td>
                                    <a class="btn btn-xs btn-primary"href="{{route('employees.edit',$employee->id)}}">Edit</a>
                                    <form action="{{ route('employees.destroy', $employee->id)}}" method="post" style="display:inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('js-per-page')
    @include('Employees/Element/index_js')
@stop
